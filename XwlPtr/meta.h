#pragma once
#include <utility>
#include <memory>
#include <type_traits>
#include <exception>

namespace helper
{
	using TrueType = char;

	using FalseType = long;
	template<typename S>
	class RecursiveRemoveAllPtr final {
		static_assert(!std::is_reference<S>::value,
			"template argument S of class RecursiveRemoveAllPtr<S> can not be of reference type");
	public:
		using Type = S;
	};

	template<typename S>
	class RecursiveRemoveAllPtr<S*> final {
		static_assert(!std::is_reference<S>::value,
			"template argument S of class RecursiveRemoveAllPtr<S> can not be of reference type");
	public:
		using Type = typename RecursiveRemoveAllPtr<typename std::remove_pointer<S>::type>::Type;
	};

	template<typename TBase, typename TDerive>
	class IsDerived final {
		static TrueType test(typename std::remove_reference<TBase>::type*);
		static FalseType test(...);

	public:
		enum {
			value = sizeof(test(
			static_cast<std::remove_reference<typename RecursiveRemoveAllPtr<TDerive>::Type>::type*>(nullptr)
			)) == sizeof(TrueType)
			&& !std::is_same<std::remove_reference<typename RecursiveRemoveAllPtr<TBase>::Type>::type,
			std::remove_reference<typename RecursiveRemoveAllPtr<TDerive>::Type>::type>::value
		};
	};

	template<typename TOrigin, typename TTarget>
	class IsImplicitlyConvertible final {
		static TrueType test(typename std::remove_reference<TTarget>::type);
		static const FalseType test(...);

	public:
		using OriginType = TOrigin;
		using TargetType = TTarget;

		enum { value = sizeof(test(std::declval<TOrigin>())) == sizeof(TrueType) };
	};

	template<bool left, bool right>
	class IgnoreLeftBool final {
	public:
		enum { value = right };
	};

	template<typename Left, typename Right>
	class IgnoreLeftType final {
	public:
		using Type = Right;
	};

	template<typename T, typename ... ArgTypes>
	class HasCtor final {
		template<typename ...CtorArgTypes>
		static const auto test(void*)
			->IgnoreLeftType<decltype(T((std::declval<CtorArgTypes>())...)), TrueType>;

		template<typename ...>
		static const FalseType test(...);

	public:
		enum { value = sizeof(test<ArgTypes...>(nullptr)) == sizeof(TrueType) };
	};
}