#pragma once
#include "meta.h"
#include "map.h"
#include "Windows.h"
#include <cinttypes>
#include <iostream>
#include <unordered_map>

using namespace std;


template<typename T>
class Ptr {
	static_assert(!std::is_reference<T>::value,
		"template argument T of class Ptr<T> can not be of reference type\ne.g. use Ptr<T> for T* and Ptr<T*> for T**");

	template<typename> friend class Ptr;

protected:
	T* m_rawPtr = nullptr;
	static LinkedMap<void*, unsigned int> m_refCountTable;
	
public:
	using Type = T;

	Ptr(T* raw)
	{
		//_CrtIsValidHeapPointer();

		if(raw != nullptr)
		{
			m_rawPtr = raw;

			auto it = m_refCountTable.find(m_rawPtr);

			if (it != m_refCountTable.end())
			{
				it->second++;
			}
			else
			{
				m_refCountTable.emplace(m_rawPtr, 1);
			}
		}
	}

	Ptr& operator=(T* raw)
	{
		//_CrtIsValidHeapPointer();

		if(raw != m_rawPtr)
		{
			if(m_rawPtr != nullptr)
			{
				auto it = m_refCountTable.find(m_rawPtr);

				if (it->second <= 1)
				{
					m_refCountTable.erase(it);
					delete m_rawPtr;
				}
				else
				{
					it->second--;
				}
			}

			m_rawPtr = raw;

			if(raw != nullptr)
			{
				auto it = m_refCountTable.find(raw);

				if (it != m_refCountTable.end())
				{
					it->second++;
				}
				else
				{
					m_refCountTable.emplace(raw, 1);
				}
			}
		}

		return *this;
	}

	//copy
	Ptr(const Ptr<T>& other)
	{
		if(other.m_rawPtr != m_rawPtr)
		{
			m_rawPtr = other.m_rawPtr;

			if (m_rawPtr != nullptr)
			{
				auto it = m_refCountTable.find(m_rawPtr);
#ifdef _DEBUG
				if(it == m_refCountTable.end())
				{
					throw runtime_error("ptr not in list");
				}
#endif
				it->second++;
			}
		}	
	}

	//copy
	Ptr& operator=(const Ptr<T>& other)
	{
		if(other.m_rawPtr != m_rawPtr)
		{
			if(m_rawPtr != nullptr)
			{
				auto it = m_refCountTable.find(m_rawPtr);
#ifdef _DEBUG
				if (it == m_refCountTable.end())
				{
					throw runtime_error("ptr not in list");
				}
#endif
				if(it->second <= 1)
				{
					m_refCountTable.erase(it);
					delete m_rawPtr;
				}
				else
				{
					it->second--;
				}
			}

			m_rawPtr = other.m_rawPtr;

			if (m_rawPtr != nullptr)
			{
				auto it = m_refCountTable.find(m_rawPtr);
#ifdef _DEBUG
				if(it == m_refCountTable.end())
				{
					throw runtime_error("ptr not in list");
				}
#endif
				it->second++;
			}
		}

		return *this;
	}


	virtual ~Ptr()
	{
		if (m_rawPtr != nullptr)
		{
			auto it = m_refCountTable.find(m_rawPtr);

#ifdef _DEBUG
			if(it == m_refCountTable.end())
			{
				throw runtime_error("trying to delete ptr that is not in list");
			}
#endif
			if (it->second <= 1)
			{
				m_refCountTable.erase(it);
				delete m_rawPtr;
			}
			else
			{
				it->second--;
			}
		}
	}

	template<typename V, typename Unused =
		typename std::enable_if<helper::IsDerived<T, V>::value, void>::type>
	Ptr(V* raw)
	{
		if(raw != nullptr)
		{
			if (!(m_rawPtr = dynamic_cast<T*>(raw)))
			{
				throw std::bad_cast("function argument type U from Ptr<T>::Ptr(const U*) is not derived from template argument T in class Ptr<T>");
			}
			else
			{
				m_rawPtr = (T*)raw;

				auto it = m_refCountTable.find(m_rawPtr);

				if (it != m_refCountTable.end())
				{
					it->second++;
				}
				else
				{
					m_refCountTable.emplace(m_rawPtr, 1);
				}

			}
		}
	}

	template<typename V, typename Unused =
		typename std::enable_if<helper::IsDerived<T, V>::value, void>::type>
	Ptr& operator=(V* raw)
	{
		if(raw != m_rawPtr)
		{
			if(raw != nullptr)
			{
				if (!(m_rawPtr = dynamic_cast<T*>(other)))
				{
					throw std::bad_cast("function argument type U from Ptr<T>::Ptr(const U*) is not derived from template argument T in class Ptr<T>");
				}
				else
				{
					m_rawPtr = (T*)raw;

					auto it = m_refCountTable.find(m_rawPtr);

					if (it != m_refCountTable.end())
					{
						it->second++;
					}
					else
					{
						m_refCountTable.emplace(m_rawPtr, 1);
					}
				}
			}
			else
			{
				auto it = m_refCountTable.find(m_rawPtr);

#ifdef _DEBUG
				if (it == m_refCountTable.end())
				{
					throw runtime_error("ptr not in list");
				}
#endif

				if (it->second <= 1)
				{
					m_refCountTable.erase(it);
					delete m_rawPtr;
				}
				else
				{
					it->second--;
				}

				m_rawPtr = nullptr;
			}
		}

		return *this;
	}

	template<typename V, typename Unused =
		typename std::enable_if<helper::IsDerived<T, V>::value, void>::type>
	Ptr(const Ptr<V>& other)
	{
		if(other.m_rawPtr != nullptr)
		{
			if (!(m_rawPtr = dynamic_cast<T*>(other.m_rawPtr)))
			{
				throw std::bad_cast("function argument type U from Ptr<T>::Ptr(const Ptr<U>&) is not derived from template argument T in class Ptr<T>");
			}
			else
			{
				m_rawPtr = (T*)other.m_rawPtr;

				auto it = m_refCountTable.find(m_rawPtr);
#ifdef _DEBUG
				if (it == m_refCountTable.end())
				{
					throw runtime_error("ptr not in list");
				}
#endif
				it->second++;
			}
		}
	}

	template<typename V, typename Unused =
		typename std::enable_if<helper::IsDerived<T, V>::value, void>::type>
	Ptr& operator=(const Ptr<V>& other)
	{
		if(m_rawPtr != (T*)other.m_rawPtr)
		{
			if(other.m_rawPtr != nullptr)
			{
				if (!(m_rawPtr = dynamic_cast<T*>(other.m_rawPtr)))
				{
					throw std::bad_cast("function argument type U from Ptr<T>::Ptr(const Ptr<U>&) is not derived from template argument T in class Ptr<T>");
				}
				else
				{
					m_rawPtr = (T*)other.m_rawPtr;
					
					auto it = m_refCountTable.find(m_rawPtr);
#ifdef _DEBUG
					if (it == m_refCountTable.end())
					{
						throw runtime_error("ptr not in list");
					}
#endif
					it->second++;
				}
			}
			else
			{
				auto it = m_refCountTable.find(m_rawPtr);

#ifdef _DEBUG
				if (it == m_refCountTable.end())
				{
					throw runtime_error("ptr not in list");
				}
#endif

				if (it->second <= 1)
				{
					m_refCountTable.erase(it);
					delete m_rawPtr;
				}
				else
				{
					it->second--;
				}

				m_rawPtr = nullptr;
			}
		}

		return *this;
	}

/*#ifndef XWL_CONFIG_PEDANTIC
#pragma warning( suppress: 4290 )
	//inplace object construction
	template<typename ...ArgTypes>
	explicit Ptr(ArgTypes ...args)
#if (_MSC_VER >= 1900) || defined(XWL_CONFIG_NOEXCEPT) || !defined(_MSC_VER)
		noexcept(helper::HasCtor<T, ArgTypes>::value, false>::value)
#else //visualcpp will ignore any throw(..) directive
		throw(typename std::enable_if<
		helper::HasCtor<T, ArgTypes>::value,
		std::exception
		>::type) //yes this is a hack and I completely abuse the syntax
#endif
		: Ptr(new T(args...))
	{
	}
#endif

#ifndef XWL_CONFIG_PEDANTIC
	template<>
#endif*/
	Ptr()
		: m_rawPtr(nullptr)
	{
	}


	//swap the raw pointers
	void swap(Ptr<T>& other)
	{
		T* temp = m_rawPtr;
		m_rawPtr = other.m_rawPtr;
		other.m_rawPtr = temp;
	}

	unsigned int getReferenceCount() const
	{
		auto it = m_refCountTable.find(m_rawPtr);

		if (it != m_refCountTable.end())
		{
			return it->second;
		}
		else
		{
			return 0;
		}
	}

	bool isUnique() const
	{
		return getReferenceCount() == 1;
	}

	T* getRawPtr() const
	{
		return m_rawPtr;
	}

	bool operator==(const T* other) const
	{
		return m_rawPtr == other;
	}

	bool operator==(decltype(nullptr)) const
	{
		return m_rawPtr == nullptr;
	}

	bool operator==(Ptr<T> other) const
	{
		return m_rawPtr == other.m_rawPtr;
	}

	operator bool() const
	{
		return (bool)m_rawPtr;
	}

	operator T*() const
	{
		return m_rawPtr;
	}

	template<typename S>
	explicit operator S*() const
	{
		return (S*)m_rawPtr;
	}

	template<typename S>
	explicit operator S() const = default;

	template<typename S>
	explicit operator Ptr<S>() const = default;

	T* operator->()
	{
		return m_rawPtr;
	}

	T& operator*()
	{
		return *m_rawPtr;
	}

	template<typename S>
	S* dynamicCast()
	{
		return dynamic_cast<S*>(m_rawptr);
	}

	//remove this reference from the list of references
	/*void operator delete()
	{
		m_refCountTable.find(m_rawPtr)->second--;
		m_rawPtr = nullptr;
	}*/

	//delete without checking if other pointers to this object still exist
	void hardDelete()
	{
		delete m_rawPtr();
	}
};

template<typename T>
LinkedMap<void*, unsigned int> Ptr<T>::m_refCountTable;

class Base {};
class Derive : public Base {};
class NoDerive {};

void foo(Ptr<Base> b)
{
}

#pragma optimize("", off)

int main(int argc, TCHAR* argv[])
{

	/*Ptr<Derive> a;
	Ptr<NoDerive> b;
	//Ptr<Base> c(a, int{});
	Ptr<Base> d(new Base());
	Ptr<Base> e(d);
	Ptr<Base> f = a;
	f = new Derive();
	f = a;
	Ptr<int> g;
	Ptr<int*> h;

	foo(a);
	foo(new Derive());
	foo(new Base());*/
	//foo(new NoDerive());
	//foo(b);

	//system("pause");

	int before = GetTickCount();

	Ptr<int>* pArr = new Ptr<int>[100000];

	for (uint64_t i = 0; i < 100000; i++)
	{
		pArr[i] = new int{12123142};
	}

	delete[] pArr;

	cout << "counted: " << GetTickCount() - before << endl; 

	before = GetTickCount();

	shared_ptr<int>* pArr2 = new shared_ptr<int>[100000];

	for(uint64_t i=0; i < 100000; i++)
	{
		pArr2[i] = make_shared<int>(12123142);
	}

	delete[] pArr2;

	cout << "counted: " << GetTickCount() - before << endl;

	system("Pause");

	return 0;
}


#pragma optimize("", on)