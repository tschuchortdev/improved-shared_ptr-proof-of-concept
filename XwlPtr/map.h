#pragma once
#include <memory>
#include <exception>
#include <iterator>
#include <array>
#include <future>

template<typename TKey, typename TValue>
class LinkedMap {
public:
	using KeyType = TKey;
	using ValueType = TValue;

protected:
	struct Element {
		Element* before;
		Element* after;
		std::pair<KeyType,ValueType> keyVal;
	};

	/* not the last element, but the element after that.
	   m_end->before is the last valid element with a value in the list */
	Element* m_end = new Element{nullptr, nullptr, {}}; 
	Element* m_first = m_end;
	size_t m_size = 0;

public:
	class MapIterator : public std::iterator<std::bidirectional_iterator_tag, std::pair<KeyType,ValueType> > {
		friend class LinkedMap;
	protected:
		Element* p;

		MapIterator(Element* p) 
			:p(p) 
		{
		}

	public:
		MapIterator(const MapIterator& mit)
			: p(mit.p)
		{
		}

		MapIterator& operator+(int a)
		{
			if(a >= 0)
			{
				for(; a > 0; a--)
				{
					operator++();
				}
			}
			else
			{
				for(; a < 0; a++)
				{
					operator--();
				}
			}

			return *this;
		}

		MapIterator& operator-(int a)
		{
			if (a >= 0)
			{
				for (; a > 0; a--)
				{
					operator--();
				}
			}
			else
			{
				for (; a < 0; a++)
				{
					operator++();
				}
			}

			return *this;
		}

		MapIterator& operator++() 
		{ 
			if (p->after == nullptr)
			{
				throw std::out_of_range("can not advance end iterator");
			}

			p = p->after; 

			return *this; 
		}

		MapIterator operator++(int) 
		{ 
			MapIterator tmp(*this); 
			operator++(); 
			return tmp; 
		}

		MapIterator& operator--()
		{
			if (p->before == nullptr)
			{
				throw std::out_of_range("can not decrease iterator because iterator points to first element or list is empty")
			}

			p = p->before;

			return *this;
		}

		MapIterator operator--(int)
		{
			MapIterator tmp(*this);
			operator--();
			return tmp;
		}
		
		bool operator==(const MapIterator& other) 
		{ 
			return p == other.p; 
		}

		bool operator!=(const MapIterator& other) 
		{ 
			return p != other.p; 
		}

		std::pair<KeyType,ValueType>& operator*() 
		{ 
			if(p->after == nullptr)
			{
				throw std::out_of_range("can not dereference end iterator");
			}

			return p->keyVal; 
		}

		std::pair<KeyType, ValueType>* operator->()
		{
			if (p->after == nullptr)
			{
				throw std::out_of_range("can not dereference end iterator");
			}

			return &p->keyVal;
		}

		bool isEndIterator() const
		{
			return p->after == nullptr;
		}

		bool isStartIterator() const
		{
			return p->before == nullptr;
		}
	};

	LinkedMap() = default;

	LinkedMap(const LinkedMap& other)
	{
		operator=(other);
	}

	LinkedMap& operator=(const LinkedMap& other)
	{
		clear();
		
		auto itBegin = other.begin();
		auto itLast = other.end() - 1;

		//copy first element
		m_first = new Element;
		m_first->before = nullptr;
		m_first->keyVal = *itBegin;
		m_size = 1;

		//copy all elements in between
		auto before = m_first;

		for (auto it = itBegin; it != itLast - 1; it++)
		{
			auto temp = new Element;
			temp->keyVal = *it;
			temp->before = before;
			before->after = temp;

			before = temp;
			m_size++;
		}

		//copy last element
		m_last = new Element;
		m_last->before = before;
		m_last->after = m_end;
		m_end->before = m_last;
		m_last->keyVal = *itLast;
		m_size++;

		return *this;
	}

	LinkedMap(LinkedMap&& other)
	{
		operator=(other);
	}

	LinkedMap& operator=(LinkedMap&& other)
	{
		clear();
		
		m_first = other.m_first;
		other.m_first = nullptr;

		m_end = other.m_end;
		other.m_end = nullptr;

		m_size = other.m_size;
		other.m_size = 0;

		return *this;
	}

	/*LinkedMap(const std::pair<KeyType,ValueType>& ...args)
	{
		std::array<typename std::pair<KeyType,ValueType>, sizeof...(args)> tempArr { args... };

		//copy first element
		m_first = new Element;
		m_first->keyVal = tempArr[0];
		m_size = 1;

		//copy all elements in between
		auto before = m_first;

		for(int i=1; i < tempArr.size()-1; i++)
		{
			auto temp = new Element;
			temp->keyVal = tempArr[i]; 
			temp->before = before;
			before->after = temp;

			before = temp;
			m_size++;
		}

		//copy last element
		m_last = new Element;
		m_last->before = before;
		m_last->after = m_end;
		m_end->before = m_last;
		m_last->keyVal = tempArr[tempArr.size()-1];
		m_size++;	
	}*/

	LinkedMap(const std::initializer_list<std::pair<KeyType,ValueType> >& il)
	{
		operator=(il);
	}

	LinkedMap& operator=(const std::initializer_list<std::pair<KeyType, ValueType> >& il)
	{
		clear();

		m_first = new Element;
		m_first->keyVal = *il.begin();
		m_first->before = nullptr;
		m_size = 1;

		auto before = m_first;
		auto itLast = il.end()--;

		for (std::initializer_list<std::pair<KeyType, ValueType> >::iterator it = il.begin(); it != itLast; it++)
		{
			auto temp = new Element;
			temp->keyVal = *it;
			temp->before = before;
			before->after = temp;

			before = temp;
			m_size++;
		}

		//copy last element
		m_last = new Element;
		m_last->before = before;
		m_last->after = m_end;
		m_end->before = m_last;
		m_last->keyVal = *itLast;
		m_size++;

		return *this;
	}

	virtual ~LinkedMap()
	{
		clear();
		delete m_end;
	}

	MapIterator begin() const
	{
		return MapIterator(m_first);
	}

	MapIterator end() const
	{
		return MapIterator(m_end);
	}

	MapIterator find(const KeyType& key) const
	{
		Element* current = m_first;

		for (unsigned int i = 0; i < m_size; ++i)
		{
			if (current->keyVal.first == key)
			{
				return MapIterator(current);
			}

			current = current->after;
		}

		return end();
	}

	MapIterator emplace(const KeyType& key, const ValueType& value)
	{
#ifdef _DEBUG
		if(find(key) != end())
		{
			throw std::invalid_argument("can not emplace element in LinkedMap because key already exists");
		}
#endif

		auto temp = new Element;

		if(m_size > 0)
		{	
			m_end->before->after = temp;
			temp->before = m_end->before;
			temp->after = m_end;
			m_end->before = temp;
		}
		else
		{
			m_first = temp;
			m_first->before = nullptr;
			m_first->after = m_end;
			m_end->before = m_first;
		}

		temp->keyVal = { key, value };
		m_size++;

		return MapIterator(temp);
	}

	MapIterator insert(const std::pair<KeyType,ValueType>& keyValPair)
	{
#ifdef _DEBUG
		if (find(keyValPair.first) != end())
		{
			throw std::invalid_argument("can not insert element in LinkedMap because key already exists");
		}
#endif
		auto temp = new Element;
		m_end->before->after = temp;
		temp->before = m_end->before;
		temp->after = m_end;
		m_end->before = temp;
		*temp->p.keyVal = keyValPair;
		m_size++;

		return MapIterator(temp);
	}

	void erase(MapIterator it)
	{
		if(it.p->before != nullptr)
		{
			it.p->before->after = it.p->after;
		}

		if(it.p->after != nullptr)
		{
			it.p->after->before = it.p->before;
		}

		delete it.p;
		m_size--;
	}

	//does nothing if key not found
	void erase(const KeyType& key)
	{
		auto it = find(key);

		if(it != end())
		{
			erase(it);
		}
		else
		{
			throw std::invalid_argument("can not erase non-existent element");
		}
	}

	//swap the values that are associated with the keys (not the pair!)
	void swap(MapIterator it1, MapIterator it2)
	{
		auto temp = it1->keyVal;
		it1->keyVal = it2->keyVal;
		it2->keyVal = it1->keyVal;
	}

	//swap the values that are associated with the keys (not the pair!)
	void swap(KeyType& key1, KeyType& key2)
	{
		auto it1 = find(key1);
		auto it2 = find(key2);

		if(it1 == end() || it2 == end())
		{
			throw std::invalid_argument("key not found in LinkedMap");
		}

		swap(it1, it2);
	}

	void swap(LinkedMap& other)
	{
		tempFirst = m_first;
		tempLast = m_last;
		tempSize = m_size;

		m_first = other.m_first;
		m_last = other.m_last;
		m_size = other.m_size;

		other.m_first = tempFirst;
		other.m_last = tempLast;
		other.m_size = tempSize;
	}

	void clear()
	{
		Element* tempAfter;
		Element* current = m_first;

		for(unsigned int i=0; i < m_size; i++)
		{
			tempAfter = current->after;
			delete current;
			current = tempAfter;
		}
	}

	size_t size() const
	{
		return m_size;
	}

	std::pair<KeyType, ValueType>& operator[](int index)
	{
		return at(index);
	}

	const std::pair<KeyType, ValueType>& operator[](int index) const
	{
		return at(index);
	}

	ValueType& operator[](const KeyType& key)
	{
		return at(key);
	}

	const ValueType& operator[](const KeyType& key) const
	{
		return at(key);
	}

	ValueType& at(const KeyType& key)
	{
		auto it = find(key);

		if(it == end())
		{
			throw std::invalid_argument("key not found in list");
		}

		return it->second;
	}

	const ValueType& at(const KeyType& key) const
	{
		return at(key);
	}

    std::pair<KeyType, ValueType>& at(unsigned int index)
	{
		if(index > m_size-1)
		{
			throw std::out_of_range("index is greater than MappedList size");
		}

		if(index <= m_size/2)
		{
			auto it = begin();

			for(int i = 0; i != index; i++)
			{
				it++;
			}
		}
		else
		{
			auto it = end()--;

			for(int i = m_size; i != index; i--)
			{
				it--;
			}
		}

		return *it;
	}

	const std::pair<KeyType, ValueType>& at(unsigned int index) const
	{
		return at(index);
	}
};